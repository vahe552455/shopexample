<?php

namespace App\Http\Middleware;

use Closure;

class Seller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()) {
            return redirect()->route('home');
        }
     else {
            if(auth()->user()->role == 'seller') {
                return $next($request);
            }
        }
        return redirect()->back();

    }
}
