<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Image;
use Image as Img;
use App\Models\Product;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function head($id ,$img)
    {
        $image = Image::findorFail($img);
        $image->product->update(['img'=>$image->name]);
        return redirect()->route('product.index');
    }
    public function delete($id,$img)
    {
        $images = Image::delete_img($img);
        if($images->name == Product::find($id)->img) {
            $data =  Product::find($id);
            $data->img = 'noimage.jpg';
            $data->save();
        }
        return response()->json(array($images));

    }
    public function image($id)
    {
        $images = Product::findorFail($id)->images();

        return view('seller.images',compact('images'));
    }
    public function index()
    {
        return view('seller.product');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('seller.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'name'=>'required',
                'cost'=>'required|numeric',
                'description'=>'required',
                'img'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]
        );
        $product = Product::create([
            'name'=>$request->name,
            'cost'=>$request->cost,
            'description'=>$request->description,
            'user_id'=>auth()->user()->id,
            'img'=>'noimage.jpg',
        ]);
        if($request->images[0]) {
            foreach ($request->images as $image) {
                $name = $image->getClientOriginalName();
                Image::create([
                    'name'=>$name,
                    'product_id'=>$product->id,
                ]);
                if(!file_exists(public_path().'\uploads\thumb\\')) {
                    File::makeDirectory(public_path().'\uploads\thumb\\',0777,true);
                }
                $fullpath = public_path().'\uploads\\'.$name;
                if(!file_exists($fullpath)) {
                    $thumbpath = public_path().'\uploads\thumb\\'.$name;
                    list($width, $height) = getimagesize($image);
                    $img = Img::make($image);
                    $img->save($fullpath ,60);
                    if($width>$height) {
                        $img->resize(300,200)->save( $thumbpath,30);
                    }
                    else {
                        $img->resize(200,300)->save( $thumbpath,30);
                    }
                }
            }
        }
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data =  Product::findorFail($id);
        return view('seller.edit',['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Product::findorFail($id);
        $data->update($request->all());
        if($request->images[0]) {
            foreach ($request->images as $image) {
                $name = $image->getClientOriginalName();
                Image::create([
                    'name' => $name,
                    'product_id' => $id,
                ]);
                if (!file_exists(public_path() . '\uploads\thumb\\')) {
                    File::makeDirectory(public_path().'\uploads\thumb\\', 0777, true);
                }
                $fullpath = public_path() . '\uploads\\' . $name;
                if (!file_exists($fullpath)) {
                    $thumbpath = public_path() . '\uploads\thumb\\' . $name;
                    list($width, $height) = getimagesize($image);
                    $img = Img::make($image);
                    $img->save($fullpath, 60);
                    if ($width > $height) {
                        $img->resize(300, 200)->save($thumbpath, 30);
                    } else {
                        $img->resize(200, 300)->save($thumbpath, 30);
                    }
                }
            }
        }
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prod = Product::delete_product($id);
        return response()->json(array($prod));
    }
}
