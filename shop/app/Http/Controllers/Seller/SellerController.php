<?php

namespace App\Http\Controllers\Seller;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SellerController extends Controller
{
    public function index() {
        return view('seller.index');
    }
    public function myOrders() {
        return view('seller.orders');
    }
    public function destroy($id) {
        $data = Order::findorFail($id);
        $data->delete();
        return response()->json(array($data));
    }

    public function confirm(Request $request) {
        $order = Order::find($request->id);
        // skzbus bolor orderneri confirmed@ 2 a ete seller@ confirm a anum darnum a 0 u order@ haytnvum a admini cucakum
        if($order->confirmed == 2) {
            $order->confirmed = 0;
            $order->save();
        }
        return redirect()->back();
    }
}
