<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

use App\Models\Image;
use App\Models\Order;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function products()
    {
        return view('admin.products');
    }
    public function orders()
    {
        return view('admin.orders');
    }
    public function index()
    {
        return view('admin.index');
    }
    public function customers() {
        return view('admin.customers');
    }
    public function sellers() {
        return view('admin.sellers');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->type == 'product') {
            $data = Product::findorFail($id);
        }else {
            $data = Order::findorFail($id);
        }
        if($data->confirmed == 0) {
            $data->confirmed = 1;
        }
        else {
            $data->confirmed = 0;
        }
        $data->save();
    return redirect()->back();
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $from  )
    {
        if($from  == 'order') {
            $data = Order::find($id);
            $data->delete();
    } elseif($from  == 'user') {
            $data = User::find($id);

            if($data->role == 'seller') {
                $data->products()->delete();
            }
            else {
                $data->orders()->delete();
            }
            $data->delete();
        }
    else {
        $data = Product::delete_product($id);
    }
        return response()->json(array($data));
    }
}
