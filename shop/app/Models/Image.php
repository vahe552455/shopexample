<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['name','product_id'];

    public function product() {
       return $this->belongsTo('App\Models\Product');
    }

    public static function delete_img($img) {
        $images = self::findorFail($img);
        if(self::where('name',$images->name)->count()<=1) {
            unlink(public_path('uploads/'.$images->name));
            unlink(public_path('uploads/thumb/'.$images->name));
        }
        $images->delete();
        return $images;
    }


}
