<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','cost','description','img','user_id','confirmed'];

    public function images() {
        return  $this->hasMany('App\Models\Image');
    }
    public function user() {
        return  $this->belongsTo('App\Models\User');
    }
 public function orders() {
        return  $this->hasMany('App\Models\Order');
    }


    public static function delete_product($id)
    {
        $prod = self::findorFail($id);
        if($prod->images()->get()) {
            foreach ($prod->images()->get() as $img ) {
                if(Image::where('name',$img->name)->count()<=1) {
                    unlink(public_path('uploads/'.$img->name));
                    unlink(public_path('uploads/thumb/'.$img->name));
                }
            }
            $prod->images()->delete();
        }
        $prod->delete();
        return  $prod;
    }

}
