<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Admin
Route::group(['middleware'=>'admin','namespace'=>'Admin'],function (){
    Route::get('/admin/products', 'AdminController@products')->name('products.show');
    Route::get('/admin/orders', 'AdminController@orders')->name('admin.orders');
    Route::get('/admin/customers', 'AdminController@customers')->name('admin.customers');
    Route::get('/admin/sellers', 'AdminController@sellers')->name('admin.sellers');

    Route::resource('/admin', 'AdminController');
    Route::delete('/admin/{id}/{from}', 'AdminController@destroy')->name('admin.destroy');

});
// Seller
Route::group(['middleware'=>'seller','namespace'=>'Seller'],function (){
    Route::get('/seller', 'SellerController@index')->name('seller');
    Route::resource('/product','ProductController');
    Route::get('product/images/{id}','ProductController@image')->name('product.show');
    Route::get('product/{id}/images','ProductController@image')->name('product.show');
    Route::delete('product/{id}/images/{image}','ProductController@delete')->name('img.destroy');
    Route::get('product/{id}/images/{image}','ProductController@head')->name('make_header');
    Route::get('seller/order','SellerController@myOrders')->name('myOrders');
    Route::delete('seller/order/{delete}','SellerController@destroy')->name('myorder.delete');
    Route::put('seller/order','SellerController@confirm')->name('order.confirm');

});
//Customer
Route::group(['middleware'=>'customer','namespace'=>'Customer','prefix'=>'customer'],function (){
    Route::get('/', 'CustomerController@index')->name('customer');
    Route::resource('/order', 'OrderController');
});


