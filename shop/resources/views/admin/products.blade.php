@extends('layouts.app')
<style>
    img.img-fluid {
        display: inline-block;
        height:50px ;
        width:auto ;
    }
</style>
@section('content')
    <div class="container">
        @if(App\Models\Product::where('confirmed',0)->count()>0)
            <div class="row justify-content-center">
                <div class="col-xl-8 ">
                    <h3 class="text-center">Confirmable Products</h3>
                    <table class="table mt-3 jquery_table"  >
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Cost</th>
                            <th>Image</th>
                            <th>Listing</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(App\Models\Product::where('confirmed',0)->get() as $item)
                            <tr data-id="{{$item->id}}" >
                                <td>{{ $item->name}} </td>
                                <td>{{ $item->description }}</td>
                                <td>{{ $item->cost }}</td>
                                <td>
                                    <a href="{{route('product.show',['id'=>$item->id])}}" ><img src="{{asset('uploads\thumb\\'.$item->img )}}" class="img-fluid"  alt=""/>
                                    </a></td>
                                <td>
                                    <form method ='POST' action="{{route('admin.update',['id'=>$item->id])}}" class="d-inline-block" >
                                        <input type="hidden" name="type" value="product" />

                                        @method('PUT')
                                        @csrf
                                        <button class="btn btn-primary" type ='submit'> Confirm </button> </form></td>
                                <td>

                                    <a href="{{route('admin.destroy',['id'=>$item->id,'type'=>'product'])}}" class="jquery-postback btn  btn-info" data-method ='delete' onclick="return confirm('Are you sure you want to delete this item?')" > Delete </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
                <div class="col-xl-8 mt-5">
                    <h3 class="text-center"> Products</h3>

                    <table class="table mt-3 jquery_table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Cost</th>
                            <th>Owner</th>
                            <th>Image</th>
                            <th>Listing</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(App\Models\Product::all() as $item)
                            <tr data-id="{{$item->id}}">
                                <td>{{ $item->name}} </td>
                                <td>{{ $item->description}}</td>
                                <td>{{ $item->cost }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td><a href="{{route('product.show',['id'=>$item->id])}}" class=""><img src="{{asset('uploads\thumb\\'.$item->img )}}" class="img-fluid"  alt=""/></a></td>
                                {{--<td>

                                    <form method ='POST' action="{{route('admin.update',['id'=>$item->id])}}" class="d-inline-block" >
                                        @method('PUT')
                                        @csrf
                                        <button class="btn btn-primary" type ='submit'> Hide </button> </form></td>--}}
                                <td>
                                    <a href="{{route('admin.destroy',['id'=>$item->id,'type'=>'product'])}}" class="jquery-postback btn  btn-info" data-method ='delete' onclick="return confirm('Are you sure you want to delete this item?')" > Delete </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
    </div>

@endsection