@extends('layouts.app')

<style>
    img.img-fluid {
        display: inline-block;
        height:50px ;
        width:auto ;
    }
</style>
@section('content')
    <div class="container">

        @if(App\Models\Order::where('confirmed',0)->count()>=0)

            <div class="row justify-content-center">
                <div class="col-xl-8 ">
                    <h3 class="text-center">Confirmable Orders</h3>

                    <table class="table mt-3 jquery_table"  >
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Description</th>
                            <th>Cost</th>
                            <th>Image</th>
                            <th>Count</th>
                            <th>User</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach(App\Models\Order::where('confirmed',0)->get() as $item)

                            <tr data-id="{{$item->id}}" >
                                <td>{{ $item->product->name}} </td>
                                <td>{{ $item->product->description }}</td>
                                <td>{{ $item->product->cost }}</td>
                                <td>
                                    <a href="{{route('product.show',['id'=>$item->id])}}" ><img src="{{asset('uploads\thumb\\'.$item->img )}}" class="img-fluid"  alt=""/>
                                    </a></td>
                                <td>{{ $item->count }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>
                                    <form method ='POST' action="{{route('admin.update',['id'=>$item->id])}}" class="d-inline-block" >
                                        <input type="hidden" name="type" value="order" />
                                        @method('PUT')
                                        @csrf
                                        <button class="btn btn-primary" type ='submit'> Confirm </button> </form></td>
                                <td>
                                    <a href="{{route('admin.destroy',['id'=>$item->id ,'from'=>'order'])}}" class="jquery-postback btn  btn-info" data-method ='delete' onclick="return confirm('Are you sure you want to delete this item?')" > Delete </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>

                </div>
                @endif
                <div class="col-xl-8 mt-5">
                    <h3 class="text-center"> Orders</h3>

                    <table class="table mt-3 jquery_table">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Description</th>
                            <th>Cost</th>
                            <th>Image</th>
                            <th>Count</th>
                            <th>User</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(App\Models\Order::all() as $item)


                            <tr data-id="{{$item->id}}" >
                                <td>{{ $item->product->name}} </td>
                                <td>{{ $item->product->description }}</td>
                                <td>{{ $item->product->cost }}</td>

                                <td>
                                    <a href="{{route('product.show',['id'=>$item->id,'type'=>'order'])}}" ><img src="{{asset('uploads\thumb\\'.$item->img )}}" class="img-fluid"  alt=""/>
                                    </a></td>
                                <td>{{ $item->count }}</td>
                                <td>{{ $item->user->name }}</td>

                                <td>
                                    <a href="{{route('admin.destroy',['id'=>$item->id,'type'=>'order'])}}" class="jquery-postback btn  btn-info" data-method ='delete' onclick="return confirm('Are you sure you want to delete this item?')" > Delete </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
    </div>

@endsection