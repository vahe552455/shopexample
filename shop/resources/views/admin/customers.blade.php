@extends('layouts.app')

@section('content')
    <div class="container">
        @if(App\Models\User::where('role','customer')->count()>0)
            <div class="row justify-content-center">
                <div class="col-xl-8 ">
                    <h3 class="text-center">Customers</h3>

                    <table class="table mt-3 jquery_table"  >
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Mail</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(App\Models\User::where('role','customer')->get() as $user)
                            <tr data-id="{{$user->id}}" >
                                <td>{{ $user->name}} </td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>
                                <td>
                                    <a href="{{route('admin.destroy',['id'=>$user->id ,'from'=>'user'])}}" class="jquery-postback btn  btn-info" data-method ='delete' onclick="return confirm('Are you sure you want to delete this item?')" > Delete </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>

                </div>
                @endif




            </div>
    </div>

@endsection