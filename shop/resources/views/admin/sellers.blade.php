@extends('layouts.app')

@section('content')
    <div class="container">
        @if(App\Models\User::where('role','seller')->count()>0)
            <div class="row justify-content-center">
                <div class="col-xl-8 ">
                    <h3 class="text-center">Sellers</h3>

                    <table class="table mt-3 jquery_table"  >
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Mail</th>
                            <th>Role</th>
                            <th>Listing</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(App\Models\User::where('role','seller')->get() as $user)
                            <tr data-id="{{$user->id}}" >
                                <td>{{ $user->name}} </td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>

                                <td>
                                    <a href="{{route('admin.destroy',['id'=>$user->id,'type'=>'user'])}}" class="jquery-postback btn  btn-info" data-method ='delete' onclick="return confirm('Are you sure you want to delete this item?')" > Delete </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>

                </div>
                @endif




            </div>
    </div>

@endsection