@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @foreach(App\Models\Product::where('confirmed',1)->get() as $prod )
                <div class="col-3">
                    <div class="card" >
                        <img src="{{asset('uploads/'.$prod->img)}}" class="card-img-top" alt="...">
                        <div class="card-body ">
                            <h5 class="card-title">{{$prod->name}}</h5>
                            <p class="card-text">{{$prod->description}}</p>
                            <span class="card-text">{{$prod->cost}} <b>amd</b>  </span>
                            <div class="text-center">
                                <a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" data-curent="{{$prod->id}}"> Buy now</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">{{$prod->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>{{$prod->cost}} <b>amd</b></p>
                                <form action="{{route('order.store',['id'=>$prod->id])}}" method="POST">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="text" class="col-md-4 col-form-label text-md-right">Count</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="count" min ='1' max ='10' value = '1'/>
                                            <input type="hidden" name="product_id" value="{{ $prod->id }}" />

                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Buy</button>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection