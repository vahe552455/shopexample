@extends('layouts.app')
<style>
    img.img-fluid {
        display: inline-block;
        height:60px ;
        width:auto ;
    }
</style>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table mt-3 jquery_table"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Cost</th>
                        <th>Count</th>
                        <th>All Price</th>
                        <th>Image</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach(App\Models\Order::where('user_id',auth()->user()->id)->get() as $item )
                        <tr>
                            <td>{{ $item->product->name}} </td>
                            <td>{{ $item->product->description }}</td>
                            <td>{{ $item->product->cost }}</td>
                            <td>{{ $item->count }}</td>
                            <td>{{ $item->count * $item->product->cost }}</td>
                            <td><img src="{{asset('uploads\thumb\\'.$item->product->img )}}" class="img-fluid"  alt=""/></td>
                            <td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection