@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">

        <div class="col-md-8 ">
            <div class="card">
                <div class="card-header">Add Product</div>
                <div class="card-body">
                    <form method="POST" action="{{route('product.store')}} " enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label text-md-right"> Name</label>
                            <div class="col-md-6"><input id="name" type="text" name="name"  required="required" autofocus="autofocus" class="form-control"></div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">Description</label>
                            <div class="col-md-6"><textarea id="password"  name="description" required="required" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">Cost</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="cost"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="images[]" multiple/>
                            </div>
                        </div>
                        <div class="form-group row mt-2">
                            <div class="col-md-8 offset-md-4"><button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection