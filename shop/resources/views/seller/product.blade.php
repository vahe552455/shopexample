@extends('layouts.app')
<style>
    img.img-fluid {
        display: inline-block;
        height:60px ;
        width:auto ;
    }
</style>
@section('content')
    <div class="container">
        <h3 class="text-center">Products</h3>
        <a href="{{route('product.create')}}" class="btn btn-info">Add </a>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table mt-3 jquery_table"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Cost</th>
                        <th>Image</th>
                        <th>Listing</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach(auth()->user()->products()->get() as $item)
                        <tr data-id="{{$item->id}}">
                            <td>{{ $item->name}} </td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->cost }}</td>
                            <td><a href="{{route('product.show',['id'=>$item->id])}}" class=""><img src="{{asset('uploads\thumb\\'.$item->img )}}" class="img-fluid"  alt=""/></a></td>
                            <td>
                                <form method ='GET' action="{{route('product.edit',['id'=>$item->id])}}" class="d-inline-block" >
                                    @csrf
                                    <button class="btn btn-info" type ='submit'> Edit</button> </form>
                            </td>
                            <td>
                                <a href="{{route('product.destroy',['id'=>$item->id])}}" class="jquery-postback btn  btn-info" data-method ='delete' onclick="return confirm('Are you sure you want to delete this item?')" > Delete </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection