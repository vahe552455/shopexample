@extends('layouts.app')
@section('content')

    <div class="row mt-4 bg-dark">
        @if($images)
            @foreach($images->get() as $image)
                <div class="col-4  bg-secondary"  data-id="{{$image->id}}">
                    <button type="button" class="close" aria-label="Close">
                        <a href="{{route('img.destroy',['product'=>$image->Product->id,'img'=> $image->id])}}" aria-hidden="true" data-method="delete" class="jquery-postback">&times;</a>
                    </button>
                    <div class="mt-3">
                        <a href="{{route('make_header' ,['product'=>$image->product->id ,'image'=>$image->id])}}">
                            <img src="{{asset('uploads/'.$image->name)}}" alt="" class="img-fluid" >
                        </a>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-3 ">
                <div class="mt-3 img-open bg-secondary ">

                    <img src="{{asset('uploads/thumb/noimage.jpg')}}" alt="" class="img-fluid" >
                </div>
            </div>
        @endif
    </div>
@endsection
