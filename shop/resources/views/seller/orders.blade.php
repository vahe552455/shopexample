@extends('layouts.app')
<style>
    img.img-fluid {
        display: inline-block;
        height:60px ;
        width:auto ;
    }
</style>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <table class="table mt-3 jquery_table"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Cost</th>
                        <th>Count</th>
                        <th>All Price</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(auth()->user()->products()->get() as $myprod)
                        @foreach($myprod->orders()->get() as $item )
                            <tr data-id="{{$item->id}}">
                                <td>{{ $item->product->name}} </td>
                                <td>{{ $item->product->description }}</td>
                                <td>{{ $item->product->cost }}</td>
                                <td>{{ $item->count }}</td>
                                <td>{{ $item->count * $item->product->cost }}</td>
                                <td><img src="{{asset('uploads\thumb\\'.$item->product->img )}}" class="img-fluid"  alt=""/></td>

                                <td>
                                    <form method ='POST' action="{{route('order.confirm')}}" class="d-inline-block" >
                                        <input type="hidden" name="id" value="{{$item->id}}" />
                                        @method('PUT')
                                        @csrf
                                        <button class="btn btn-primary" type ='submit'> Confirm </button>
                                    </form>
                                </td>
                                <td>
                                    <a href="{{route('myorder.delete',['destroy'=>$item->id ])}}" class="jquery-postback btn  btn-info" data-method ='delete' onclick="return confirm('Are you sure you want to delete this item?')" > Delete </a>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection