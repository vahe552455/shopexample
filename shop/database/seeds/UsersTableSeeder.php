<?php
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!DB::table('users')->first()) {
            User::create([
                'name'=>'Admin',
                'password'=>bcrypt('Admin'),
                'role'=>'admin',
                'email'=>'a@mail.ru',
            ]);
            User::create([
                'name'=>'VAHE1',
                'password'=>bcrypt('123'),
                'role'=>'seller',
                'email'=>'seller@mail.ru',
            ]);
            User::create([
                'name'=>'VAHE',
                'password'=>bcrypt('123'),
                'role'=>'customer',
                'email'=>'customer@mail.ru',
            ]);
        }

    }
}
